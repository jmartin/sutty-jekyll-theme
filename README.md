# sutty-jekyll-theme

A theme with [Sutty](https://sutty.nl)'s design.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "sutty-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: sutty-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sutty-jekyll-theme

## Usage

Add the gem and this to your `_config.yml`:

```yaml
theme: sutty-jekyll-theme
sasl:
  load_paths:
  - node_modules
```

Also, install [Bootstrap 4.4](https://getbootstrap.com/) using NPM or
Yarn:

```bash
yarn add bootstrap@~4.4
```

We recommend you start a site with our base site
[skel.sutty.nl](https://0xacab.org/sutty/skel.sutty.nl/).

The [Saira](https://github.com/Omnibus-Type/Saira) font is subsetted to
latin characters.

### Customization

* **Change logo:** Add your own logo in `_includes/logo.svg`, you can
  add some metadata, check the [source
  file](https://0xacab.org/sutty/jekyll/sutty-jekyll-theme/-/blob/master/_includes/logo.svg).

* **I18n / Edit menu:** This can be done by installing
  [jekyll-locales](https://rubygems.org/gems/jekyll-locales/) and
  modifying the `_data/LANGUAGE.yml` files:

  ```yaml
  ---
  locale: English
  # Leave empty for theme default
  date_format: '%m/%d/%Y'
  # This modifies the logo metadata if any (see _includes/logo.svg)
  site:
    title: Your site name
    description: Your site description
  # Title for the articles list on the homepage
  home:
    articles: Articles
  menu:
    title: 'Menu'
    active: '(current)'
    items:
    - url: 'https://sutty.nl/'
      text: 'Sutty'
  ```

  `items` is an Array of Hashes with `url` and `text` keys.

## Contributing

Bug reports and pull requests are welcome on 0xacab at
<https://0xacab.org/sutty/sutty-jekyll-theme>. This project is intended
to be a friendly and welcoming space for collaboration, and contributors
are expected to adhere to the [code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you appreciate our work, you can donate
[Bitcoin](bitcoin:3KCV7dBgJbkTuF4Lz2BxVu6bVUf9fRyp6z?label=sutty-jekyll-theme&message=Thanks+for+sutty-jekyll-theme)
or contact us for other ways :)

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme,
run `bundle exec jekyll serve` and open your browser at
`http://localhost:4000`. This starts a Jekyll server using your
theme. Add pages, documents, data, etc. like normal to test your theme's
contents. As you make modifications to your theme and to your content,
your site will regenerate and you should see the changes in the browser
after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`,
`_sass` and `assets` will be bundled.  To add a custom directory or file
to your theme-gem, please edit the regexp in
`sutty-jekyll-theme.gemspec` accordingly.

## License

The theme is available as anti-fascist software under the terms of the
[Anti Fascist MIT License](LICENSE.txt).

